#ifndef FF_TIMEOU_H
#define FF_TIMEOU_H

#include "stdint.h"
#include "stdbool.h"

class Timeout
{
private:
    unsigned long _start = 0;
    unsigned int _timeout;
    unsigned int _times = 0;
    void (*user_onTimeout)(uint32_t times);
    unsigned long (*user_now)();

public:
    Timeout(unsigned long (*)());

    Timeout(unsigned long (*)(), uint32_t timeout);
    /**
     * @brief start timer, if timer is running, do nothing
     */
    void start();
    /**
     * @brief restart timer, if timer is running, re start the timer else do nothing
     */
    void restart();
    /**
     * @brief stop timer 
     */
    void stop();
    /**
     * @brief get runnning state
     * 
     * @return true is running invoke start method
     * @return false is not running invoke stop method
     */
    bool isRunning();
    /**
     * @brief Set the timeout value
     * 
     * @param timeout timeout value
     */
    void setTimeout(uint32_t timeout);
    /**
     * @brief get is timeout state
     * 
     * @return true timeout
     * @return false not timeout
     */
    bool isTimeout();
    /**
     * @brief get is timeout state
     * 
     * @param timeout timeout value
     * @return true timeout
     * @return false not timeout 
     */
    bool isTimeout(uint32_t timeout);
    /**
     * @brief set timeout event listener
     */
    void onTimeout(void (*)(uint32_t times));
    /**
     * @brief dispatch timeout event
     */
    void dispatchTimeoutEvent();
};

#endif